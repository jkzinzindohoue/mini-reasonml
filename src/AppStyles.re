let lightTeal = "#e0f2f1";

let style = {j|
  body {
    margin: 0;
    font-family: Roboto, sans-serif;
    background-color: $lightTeal;
    display: flex;
    flex-direction: column;
    align-items: center;
  }|j};