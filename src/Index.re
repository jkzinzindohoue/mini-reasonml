[@bs.val] external document: Js.t({..}) = "document";

let style = document##createElement("style");
document##head##appendChild(style);
style##innerHTML #= AppStyles.style;

let makeContainer = () => {
  let container = document##createElement("div");
  let () = document##body##appendChild(container);
  container;
};

// let makeContainer = text => {
//   let container = document##createElement("div");
//   container##className #= "container";

//   let title = document##createElement("div");
//   title##className #= "containerTitle";
//   title##innerText #= text;

//   let content = document##createElement("div");
//   content##className #= "containerContent";

//   let () = container##appendChild(title);
//   let () = container##appendChild(content);
//   let () = document##body##appendChild(container);

//   content;
// };

ReactDOMRe.render(<App />, makeContainer());

// All 4 examples.
// ReactDOMRe.render(
//   <BlinkingGreeting>
//     {React.string("Hello!")}
//   </BlinkingGreeting>,
//   makeContainer("Blinking Greeting"),
// );

// ReactDOMRe.render(
//   <ReducerFromReactJSDocs />,
//   makeContainer("Reducer From ReactJS Docs"),
// );

// ReactDOMRe.render(
//   <FetchedDogPictures />,
//   makeContainer("Fetched Dog Pictures"),
// );

// ReactDOMRe.render(
//   <ReasonUsingJSUsingReason />,
//   makeContainer("Reason Using JS Using Reason"),
// );