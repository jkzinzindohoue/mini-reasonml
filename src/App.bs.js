'use strict';

var React = require("react");

function App(Props) {
  var style = {
    backgroundColor: "#e0f2f1",
    height: "100vh",
    width: "100vw"
  };
  return React.createElement("main", {
              style: style
            }, React.createElement("h1", undefined, "Hello"), React.createElement("p", undefined, "Premier paragraphe"), React.createElement("p", undefined, "Second paragraphe"), React.createElement("p", undefined, "Trois\xc3\xa8me paragraphe"));
}

var make = App;

exports.make = make;
/* react Not a pure module */
