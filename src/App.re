[@react.component]
let make = () => {
  let style =
    ReactDOMRe.Style.make(
      ~backgroundColor="#e0f2f1",
      ~width="100vw",
      ~height="100vh",
      (),
    );
  <main style>
    <h1> {React.string("Hello")} </h1>
    <p> {React.string("Premier paragraphe")} </p>
    <p> {React.string("Second paragraphe")} </p>
    <p> {React.string("Troisème paragraphe")} </p>
  </main>;
};