'use strict';


var lightTeal = "#e0f2f1";

var style = "\n  body {\n    margin: 0;\n    font-family: Roboto, sans-serif;\n    background-color: " + (String(lightTeal) + ";\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n  }");

exports.lightTeal = lightTeal;
exports.style = style;
/* style Not a pure module */
